﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public Camera cam;
    Rigidbody rb;
    Vector3 movement;
    float speed = 3f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    { 
        Turning();
    }

    void FixedUpdate()
    {
        Move(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;
        rb.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;
        if (Physics.Raycast(camRay, out floorHit))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            rb.MoveRotation(newRotation);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ammo")
        {
            GetComponent<ShootPlayer>().SetAmmo(20);
            Destroy(other.gameObject);
        }
        if(other.tag == "hp")
        {
            GetComponent<PlayerHP>().Heal(50);
            Destroy(other.gameObject);
        }
       
    }
}

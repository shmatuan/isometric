﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour {

    public float dmg = 30;

    GameObject target;
    float rSearch = 2;
    Ray ray;
    RaycastHit hit;
    Rigidbody rb;

    void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	void Update () {
        if (target)
        {
            Attack();
        }
        else
        {
            Search();
            Move();
        }
    }

    float angle = 0;
    void Search()
    {
        target = GameObject.Find("Player").transform.gameObject;

        // Search
        //angle += 2 * Mathf.PI * Time.deltaTime * 3;
        //float x = Mathf.Cos(angle) * rSearch;
        //float z = Mathf.Sin(angle) * rSearch;
        //Vector3 v3 = new Vector3(x, 0, z);

        //Debug.DrawLine(transform.position, transform.position + v3, Color.red, 100f);

        //ray.origin = transform.position;
        //ray.direction = v3;
        //if (Physics.Raycast(ray, out hit, rSearch))
        //{
        //    if (hit.transform.gameObject.name == "Player")
        //    {
        //        target = hit.transform.gameObject;
        //    }
        //}
    }

    void Move()
    {

    }

    void Attack()
    {
        ray.origin = transform.position;
        ray.direction = transform.forward;
        if (Physics.Raycast(ray, out hit, 0.4f))
        {
            if (hit.transform.gameObject.name == "Player")
            {
                hit.transform.gameObject.GetComponent<PlayerHP>().Damage(dmg);
                Destroy(gameObject);
            }
        }

        /*
         * Пускать рей из обьекта в таргет 
         * при препятствеии или искать обходной, пуская по кругу рей 
         * или найти обьект, получить размер, обойти по меньшей стороне
         */

        transform.LookAt(target.transform);
        var rot = transform.localRotation;
        rot.x = 0;
        rot.z = 0;
        transform.localRotation = rot;
        Vector3 movement = transform.forward;
        movement = movement.normalized * 2 * Time.deltaTime;
        movement.y = 0;
        rb.MovePosition(transform.position + movement);
    }
}

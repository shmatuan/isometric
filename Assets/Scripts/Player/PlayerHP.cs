﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerHP : MonoBehaviour {

    public GameObject hpBar;
    public float maxHP = 100;
    float hp;
    float maxScale;

    void Start () {
        maxScale = hpBar.transform.localScale.x;
        hp = maxHP;
    }

    public void Heal(float cnt)
    {
        if(hp + cnt <= maxHP)
        {
            hp += cnt;
        }
        else
        {
            hp = maxHP;
        }
        HPBarRefresh();
    }

    public void Damage(float dmg)
    {
        hp -= dmg;
        HPBarRefresh();
        if (hp <= 0)
        {
            HPBarRefresh();
            Destroy(gameObject);
            SceneManager.LoadScene("main");
        }
    }

    
    void HPBarRefresh()
    {
        Vector3 scale = hpBar.transform.localScale;
        scale.x = maxScale *  hp / maxHP;
        hpBar.transform.localScale = scale;
    }
}

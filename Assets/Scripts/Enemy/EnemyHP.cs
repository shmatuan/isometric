﻿using UnityEngine;
using System.Collections;

public class EnemyHP : MonoBehaviour {

    public GameObject hpBar;
    public float maxHP = 25;
    float hp;

	void Start () {
        hp = maxHP;
	}

    public void Damage(int dmg)
    {
        hp -= dmg;
        HPBarRefresh();
        if (hp <= 0)
        {
            Destroy(gameObject);
        }
    }

    void HPBarRefresh()
    {
        Vector3 scale = hpBar.transform.localScale;
        scale.x *= hp / maxHP;
        hpBar.transform.localScale = scale;
    }
}

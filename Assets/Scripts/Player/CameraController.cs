﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    private GameObject player;

	void Start () {
        player = GameObject.Find("Player");
	}
	
	void Update () {
        var v3 = player.transform.position;
        v3.y = 6.5f;
        v3.z += -3f;
        transform.position = v3;
	}
}

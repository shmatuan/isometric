﻿using UnityEngine;
using System.Collections;

public class SpawnerController : MonoBehaviour {

    public GameObject mobEye;
    public GameObject ammo;
    public GameObject hp;

    private GameObject mobSpawners;
    private GameObject htSpawners;
    private GameObject enemies;
    private float timerMob;
    private float timeMobSpawn = 1f;

    private float timerHT;
    private float timeHTSpawn = 5f;

// Давать деньги за время

    void Start () {
        mobSpawners = GameObject.Find("Spawners");
        htSpawners = GameObject.Find("HTSpawners");
        enemies = GameObject.Find("Enemies");
    }
	
	void Update () {
        Tick();        
	}

    void Tick()
    {
        if (timerMob < timeMobSpawn)
        {
            timerMob += Time.deltaTime;
        }
        else
        {
            SpawnMob();
            timerMob = 0;
        }

        if (timerHT < timeHTSpawn)
        {
            timerHT += Time.deltaTime;
        }
        else
        {
            SpawnHT();
            timerHT = 0;
        }
    }

    void SpawnMob()
    {
        Transform spawn = mobSpawners.transform.GetChild(Random.Range(0, mobSpawners.transform.childCount));

        GameObject mob = mobEye;
        mob.transform.position = spawn.position;
        mob = Instantiate(mob);
        mob.transform.parent = enemies.transform;
    }

    void SpawnHT()
    {
        Transform spawn = htSpawners.transform.GetChild(Random.Range(0, htSpawners.transform.childCount));

        GameObject ht = Random.Range(0, 2) == 0 ? ammo : hp;
        ht.transform.position = spawn.position;
        ht = Instantiate(ht);
        ht.transform.parent = enemies.transform;
    }
}

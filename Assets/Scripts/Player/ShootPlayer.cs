﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShootPlayer : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.25f;
    public float range = 100f;
    public int ammoPerShoot = 1;

    GameObject gun;
    float timer;
    float effectsDisplayTime = 0.2f;
    Ray shootRay;
    RaycastHit shootHit;
    ParticleSystem gunParticles;
    LineRenderer gunLine;

    Text ammoText;
    GameObject ammoReload;
    public float gunCurrentAmmo;
    public float gunMaxAmmo = 15;
    public float currentAmmo;
    public float maxAmmo = 100;
    public float reloadTime = 1f;
    bool reload = false;

    void Awake()
    {
        gun = GameObject.Find("Player/Gun").gameObject;
        gunParticles = gun.GetComponent<ParticleSystem>();
        gunLine = gun.GetComponent<LineRenderer>();

        ammoText = GameObject.Find("MainCanvas/Ammo").GetComponent<Text>();
        ammoReload  = GameObject.Find("MainCanvas/AmmoReload");
        gunCurrentAmmo = gunMaxAmmo;
       // currentAmmo = maxAmmo;

        UpdUI();
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            if (SetGunAmmo(ammoPerShoot) && !reload)
            {
                Shoot();
                timer = 0f;
                UpdUI();
            }
        }
        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            gunLine.enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.R) || reload)
        {
            if (!reload) timer = 0f;
            ReloadGun();

            Vector3 scale = ammoReload.transform.localScale;
            if (timer / reloadTime > 0) scale.x = 1 - timer / reloadTime;
            else scale.x = 0;
            ammoReload.transform.localScale = scale;
        }
    }

    void UpdUI()
    {
        ammoText.text = gunCurrentAmmo + "/" + currentAmmo;
    }

    void Shoot()
    {
        gunParticles.Stop();
        gunParticles.Play();
        gunLine.enabled = true;
        gunLine.SetPosition(0, gun.transform.position);
        shootRay.origin = gun.transform.position;
        shootRay.direction = gun.transform.forward;
        if (Physics.Raycast(shootRay, out shootHit, range))
        {
            EnemyHP enemyHP = shootHit.collider.GetComponent<EnemyHP>();
            if (enemyHP) enemyHP.Damage(damagePerShot);

            gunLine.SetPosition(1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
        }
    }

    public bool SetGunAmmo(int cnt)
    {
        if (gunCurrentAmmo - cnt >= 0)
        {
            gunCurrentAmmo -= cnt;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ReloadGun()
    {
        if (currentAmmo > 0)
        {
            reload = true;
            if (timer >= reloadTime)
            {
                if (currentAmmo > 0)
                {

                    float gunammo = gunCurrentAmmo;

                    if (currentAmmo - gunMaxAmmo + gunCurrentAmmo >= 0)
                    {
                        currentAmmo = currentAmmo - gunMaxAmmo + gunCurrentAmmo;
                        gunCurrentAmmo = gunMaxAmmo;
                    }
                    else
                    {
                        gunCurrentAmmo += currentAmmo;
                        currentAmmo = 0;
                    }
                }
                UpdUI();
                timer = 0f;
                reload = false;
            }
        }
    }

    public void SetAmmo(float cnt)
    {
        if (currentAmmo + cnt <= maxAmmo)
        {
            currentAmmo += cnt;
        }
        else
        {
            currentAmmo = maxAmmo;
        }
        UpdUI();        
    }
}
